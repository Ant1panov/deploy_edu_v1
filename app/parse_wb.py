import pandas as pd
import requests
import os
import sqlalchemy


def get_category():
    headers = {
        'Accept': '*/*',
        'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'Connection': 'keep-alive',
        'Origin': 'https://www.wildberries.ru',
        'Referer': 'https://www.wildberries.ru/brands/tecno',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'cross-site',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }
    url = "https://catalog.wb.ru/brands/t/catalog?appType=1&brand=23233&curr=rub&dest=-1257786&regions=80,38,83,4,64,33,68,70,30,40,86,75,69,22,1,31,66,110,48,71,114&sort=popular&spp=0&uclusters=0"
    response = requests.get(url=url, headers=headers)
    return response.json()


def prepare_items(response):
    products = []
    products_raw = response.get('data', {}).get('products', None)
    if products_raw is not None and len(products_raw) > 0:
        for product in products_raw:
            products.append(
                {
                    "brand": product.get('brand', None),
                    "name": product.get("name", None),
                    "sale": product.get("sale", None),
                    # "priceU": float(product.get('PriceU', None)) / 100 if product.get("priceU", None) != None else None,
                    # "salePriceU": float(product.get("salePriceU", None)) / 100 if product.get("salePriceU", None) != None else None,
                }
            )
    return products


def main():
    response = get_category()
    products = prepare_items(response)
    pd.DataFrame(products).to_csv('product.csv', index=False)
    my_df_file = pd.read_csv('product.csv', delimiter=',')
    print(my_df_file)
    url_db = os.environ["SQLALCHEMY_DATABASE_URI"]
    engine = sqlalchemy.create_engine(url_db)
    with engine.connect() as con:
        my_df_file.to_sql(
            name="products",
            con=con,
            # chunksize=1000,
            # method='multi',
            index=False,
            if_exists='replace'
        )


if __name__ == "__main__":
    main()