from flask import Flask, render_template, request, flash
from extensions import extensions
import parse_wb
# from routes.routes import home
import os
# from dash_application import create_dash_application
# from flask_login import LoginManager
# from models.models import users
# import os
# db_dir = os.path.abspath(os.path.dirname(__file__)) + "/database"
# from flask_login import login_user, login_required
import pandas as pd
import requests
import sqlalchemy

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:123456@localhost:5432/fms_v3_db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:123456@db:5432/postgres'

# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(db_dir, 'datab.db')
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:zh2311@db:5432/postgres'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'secret string'


# # os.environ["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + os.path.join(db_dir, 'datab.db')
os.environ["SQLALCHEMY_DATABASE_URI"] = 'postgresql://postgres:123456@db:5432/postgres'
# # os.environ["SQLALCHEMY_DATABASE_URI_PSYCORG2"] = 'postgresql+psycopg2://postgres:zh2311@db:5432/postgres'
#
#
#
db = extensions.db
#
# migrate = extensions.migrate
# socketio = extensions.socketio
#
# create_dash_application(app)

# socketio.init_app(app)
#
db.init_app(app)
#
# app.register_blueprint(home)

# login_manager = LoginManager()
# login_manager.login_view = 'home.login'
# login_manager.init_app(app)

# @login_manager.user_loader
# def load_user(user_id):
#     # since the user_id is just the primary key of our user table, use it in the query for the user
#     return users.query.get(int(user_id))


@app.route("/")
def home():
    return render_template('index.html')

work = "готово"


@app.route("/parsing/", methods=['POST'])
def move_forward():
    parse_wb.main()
    my_df_file = pd.read_csv('product.csv', delimiter=',')
    return render_template('index.html', work=work)


app._favicon = "favicon.ico"


if __name__ == '__main__':
    app.run(debug=True)