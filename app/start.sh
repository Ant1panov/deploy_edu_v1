#!/bin/bash

gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 --timeout 6000 --reload -b 0.0.0.0:8000 app:app
